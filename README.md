# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git init, git remote add stroboskop https://bitbucket.org/Fedj/stroboskop, git clone https://bitbucket.org/Fedj/stroboskop

Naloga 6.2.3:

https://bitbucket.org/Fedj/stroboskop/commits/2f0c2054066e22d2927a373aca1a2884beaaf4d0

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:

https://bitbucket.org/Fedj/stroboskop/commits/229c4f856d8ac19f9506c63ae25045e4e8c4bd9e

Naloga 6.3.2:

https://bitbucket.org/Fedj/stroboskop/commits/67d5881ad4124d6d54a8ab2fb009c0abddbf68ab

Naloga 6.3.3:

https://bitbucket.org/Fedj/stroboskop/commits/5e64205a341179a5c4a1d3de504ba12791e704b4

Naloga 6.3.4:

https://bitbucket.org/Fedj/stroboskop/commits/300673ba5a06539d0becb13e957f18483626bacb

Naloga 6.3.5:

git checkout master, git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:

https://bitbucket.org/Fedj/stroboskop/commits/51dc1f29ef81c6de1796da31e82c1308aa3410f9

Naloga 6.4.2:

https://bitbucket.org/Fedj/stroboskop/commits/d47934ef72553afbe52b0dbc953b67e9cc21b822

Naloga 6.4.3:

https://bitbucket.org/Fedj/stroboskop/commits/76c8b6b7ad6784b2be63adbc20505df2e3ae473b

Naloga 6.4.4:

https://bitbucket.org/Fedj/stroboskop/commits/0b8592f762aa315479274fccb8f645cc6308a0c3